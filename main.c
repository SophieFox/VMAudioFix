#include <stdlib.h>
#include <stdio.h>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#define STATUS_SUCCESS 0
#define STATUS_TIMER_RESOLUTION_NOT_SET 0xC0000245
NTSYSAPI int NTAPI NtSetTimerResolution(ULONG DesiredResolution, BOOLEAN SetResolution, PULONG CurrentResolution);
NTSYSAPI int NTAPI NtQueryTimerResolution(ULONG* min, ULONG* max, ULONG* current);

int vasprintf(char **strp, const char *fmt, va_list ap);
int asprintf(char **strp, const char *fmt, ...);

int CALLBACK WinMain(HINSTANCE inst, HINSTANCE prevInst, LPSTR cmdline, int cmdShow) {
	ULONG resMin, resMax, resCurrent, desired;
	int queryOnly = FALSE, quiet = FALSE;
	printf("\n  VMAudioFix (c) 2017 by Hendrik 'Sharky' Schumann\n  https://schumann.pw/  \n\n");

	if(__argc == 2) {
		if(strcmpi(__argv[1], "query") == 0) {
			queryOnly = TRUE;
			printf("Query only, not setting timer!\n");
		} else if(strcmpi(__argv[1], "quiet") == 0) {
			quiet = TRUE;
		} else {
			desired = atol(__argv[1]);
			printf("Desired timer resolution by argument: %lu\n", desired);
		}
	}

	if(NtQueryTimerResolution(&resMin, &resMax, &resCurrent) != STATUS_SUCCESS) {
		fprintf(stderr, "Failed to query timer resolution!\n");
		return 1;
	}
	desired = resMax;

	printf("NtQueryTimerResolution:\n  - lowest: %lu\n  - highest: %lu\n  - current: %lu\n", resMin, resMax, resCurrent);

	if(queryOnly) {
		return 0;
	}

	int status = NtSetTimerResolution(desired, TRUE, &resCurrent);
	char* statusMsg = status == STATUS_SUCCESS ? "success" : "error";
	printf("\nNtSetTimerResolution: %s, set to: %luns", statusMsg, resCurrent);

	if(quiet == FALSE) {
		char *msg;
		asprintf(&msg, "nNtSetTimerResolution: %s, set to: %luns", statusMsg, resCurrent);
		MessageBox(GetConsoleWindow(), msg, "VMAudioFix", MB_OK| (status == STATUS_SUCCESS ? MB_ICONINFORMATION : MB_ICONERROR ) );
		free(msg);
	}

	printf("\nKeep this program running or press any key to exit.\n");
	fflush(stdout);

	if(quiet) {
		ShowWindow(GetConsoleWindow(), SW_HIDE);
	}

	getchar();

	return 0;
}

// thanks https://stackoverflow.com/a/19409495
int vasprintf(char **strp, const char *fmt, va_list ap)
{
    va_list ap1;
    size_t size;
    char *buffer;

    va_copy(ap1, ap);
    size = vsnprintf(NULL, 0, fmt, ap1) + 1;
    va_end(ap1);
    buffer = calloc(1, size);

    if (!buffer)
        return -1;

    *strp = buffer;

    return vsnprintf(buffer, size, fmt, ap);
}

int asprintf(char **strp, const char *fmt, ...)
{
    int error;
    va_list ap;

    va_start(ap, fmt);
    error = vasprintf(strp, fmt, ap);
    va_end(ap);

    return error;
}
